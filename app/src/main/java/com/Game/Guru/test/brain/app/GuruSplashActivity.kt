package com.Game.Guru.test.brain.app

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Game.Guru.test.brain.app.databinding.ActivityGuruSplashBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")
class GuruSplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGuruSplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGuruSplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        GlobalScope.launch {
            delay(2850)
            val intent = Intent(this@GuruSplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}