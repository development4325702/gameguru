package com.Game.Guru.test.brain.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.Game.Guru.test.brain.app.databinding.ActivityHaloBinding

class HaloActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHaloBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHaloBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()
        var correctAnswer = intent.getIntExtra("score", 0)

        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.green))
            correctAnswer++
            binding.button3.isClickable = false
            binding.button1.isClickable = false
        }

        binding.button3.setOnClickListener {
            binding.button3.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button1.isClickable = false
        }

        binding.nextBtn.setOnClickListener {
            binding.resultLayout.visibility = View.VISIBLE
            binding.scoreValue.text = correctAnswer.toString()
            binding.nextBtn.text = "to menu"
            binding.nextBtn.setOnClickListener {
                finish()
            }
            binding.mainConstraint.setOnClickListener {
                binding.resultLayout.visibility = View.GONE
            }
        }
        binding.hamb.setOnClickListener {
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}